import React from "react";
import { YellowBox } from "react-native";

//Ocultando o Warning de incompatibilidade do WebSoket com o RN
//ele esta tentando usar uma proprieade do Browser que não existe aqui no RN
YellowBox.ignoreWarnings(["Unrecognized WebSocket"]);

import Routes from "./routes";

const App = () => <Routes />;

export default App;
